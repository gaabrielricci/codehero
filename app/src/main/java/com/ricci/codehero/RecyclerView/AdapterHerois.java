package com.ricci.codehero.RecyclerView;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ricci.codehero.Model.Heroi;
import com.ricci.codehero.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class AdapterHerois extends RecyclerView.Adapter<AdapterHerois.ViewHolder>{
    private ArrayList<Heroi> heroiLista;
    private Activity activity;

    public AdapterHerois(ArrayList<Heroi> heroiLista, Activity activity) {
        this.activity = activity;
        this.heroiLista = heroiLista;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new ViewHolder(LayoutInflater.from(activity).inflate(R.layout.card_heroi,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        try {

            Heroi heroi = heroiLista.get(position);
            holder.tv_nome.setText(heroi.getNome());
            Picasso.get()
                    .load(heroi.getFoto())
                    .error(R.mipmap.ic_alerta)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(holder.foto);
        }
        catch (Exception e)
        {
            Log.e("Erro:",e.toString());
        }
    }

    @Override
    public int getItemCount() {
        return heroiLista == null ? 0 : heroiLista.size();
    }



    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tv_nome;
        private CircleImageView foto;

        private ViewHolder(View itemView) {
            super(itemView);

            tv_nome = itemView.findViewById(R.id.card_nome_heroi);
            foto = itemView.findViewById(R.id.card_foto_heroi);
        }

        @Override
        public void onClick(View v)
        {
            switch (v.getId())
            {

            }

        }
    }





}
