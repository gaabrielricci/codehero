package com.ricci.codehero;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.koushikdutta.ion.Ion;

import java.util.Date;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }






    private void baixaDados(){

        long time = new Date().getTime();

        Ion.with(this)
                .load(getString(R.string.API)+"/"+getString(R.string.PERSONAGENS))
                .addQuery("ts",String.valueOf(time))
                .addQuery("apiKey",getString(R.string.PUBLIC_KEY));
    }


}
