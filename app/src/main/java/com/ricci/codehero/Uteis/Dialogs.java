package com.ricci.codehero.Uteis;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.ricci.codehero.R;


public class Dialogs {

    private AlertDialog ad;
    private Context context;

    public Dialogs(Context context) {
        this.context=context;
    }

    public AlertDialog getAd() {
        return ad;
    }

    public  void alertLoad(@Nullable String sTexto, ViewGroup viewGroup)
    {
        ad = new AlertDialog.Builder(context).create();

        LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert li != null;
        final View dialoggar = li.inflate(R.layout.alert_load, viewGroup,false);
        ad.setView(dialoggar);

        setTextoLoad(sTexto);
        ad.setCancelable(false);

        if(ad.getWindow()!=null){
            ad.getWindow().getAttributes().windowAnimations = R.style.AppTheme_DialogZoom;
            ad.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        ad.show();
    }


    private void setTextoLoad(String textoLoad){
        if(textoLoad!=null){
            try{
                TextView texto = ad.findViewById(R.id.text_texto);
                assert texto != null;
                texto.setText(textoLoad);
            }catch (Exception e){
                Log.e("alertLoad",e.toString());
            }
        }
    }

    public void cancelAd(){
        if(ad!=null) ad.cancel();
    }



}
